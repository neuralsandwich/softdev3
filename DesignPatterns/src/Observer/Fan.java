package com.neuralsandwich.PopStars;

public class Fan implements Observer, DisplayElement{

	private String name;
	private String news;
	
	public Fan(String n) {
		this.name = n;
	}

	@Override
	public void update(String news) {
		this.news = news;
		display();
	}

	@Override
	public void display() {
		System.out.println(this.name + "'s news feed");
		System.out.println(news);		
	}

	
	
}
