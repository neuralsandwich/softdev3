package com.neuralsandwich.PopStars;

import java.util.ArrayList;

public class PopStar implements Observable{

	private String news;

	private ArrayList <Fan> adoringFans = new ArrayList<Fan>();

	@Override
	public void registerObserver(Fan f) {
		this.adoringFans.add(f);		
	}

	@Override
	public void unregisterObserver(Fan f) {
		this.adoringFans.remove(f);
	}

	@Override
	public void notifyObservers() {
		for (Fan tempFan : adoringFans){
			tempFan.update(news);
		}
	}

	public void setNews(String news) {
		this.news = news;
	}

	
}
