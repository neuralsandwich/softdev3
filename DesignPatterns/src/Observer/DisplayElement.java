package com.neuralsandwich.PopStars;

public interface DisplayElement {
	public void display();
}
