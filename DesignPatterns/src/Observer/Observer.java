package com.neuralsandwich.PopStars;

public interface Observer {

	public void update(String news);
}
