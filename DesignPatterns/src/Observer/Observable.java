package com.neuralsandwich.PopStars;

public interface Observable {
	public void registerObserver(Fan f);
	public void unregisterObserver(Fan f);
	public void notifyObservers();
}
