package com.neuralsandwich.PopStars;

public class Main {

	public static void main(String[] args) {
		PopStar ps = new PopStar();
		Fan f1 = new Fan("Jill");
		Fan f2 = new Fan("Jack");
		Fan f3 = new Fan("Claire");
		
		ps.registerObserver(f1);
		ps.registerObserver(f2);
		ps.registerObserver(f3);

		ps.setNews("I have a new Album!");
		ps.notifyObservers();
		
		ps.unregisterObserver(f2);
		
		ps.setNews("I am on Tour!");
		ps.notifyObservers();

	}

}
