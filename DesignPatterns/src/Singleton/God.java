package com.neuralsandwich.universe;

public class God {

	public static void main(String[] args) {
		Universe u1 = Universe.getInstance();
		Universe u2 = Universe.getInstance();
		
		if (u1.equals(u2)){
			System.out.println("Both Universes are the same");
		} else {
			System.out.println("Well Looks like the multiverse theory is right!");
		}

	}

}
