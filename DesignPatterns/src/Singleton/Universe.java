package com.neuralsandwich.universe;

public class Universe {
	
	private static Universe uniqueUniverse;
	
	private Universe() {}

	public static synchronized Universe getInstance(){
		if (uniqueUniverse == null) {
			uniqueUniverse = new Universe();
		}
		
		return uniqueUniverse;
	}

}
