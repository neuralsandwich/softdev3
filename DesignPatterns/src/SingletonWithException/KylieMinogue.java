package com.neuralsandwich.KylieMinogue;

public class KylieMinogue {
	
	private static final KylieMinogue kylieMinogue = new KylieMinogue();
	
	private KylieMinogue() {
		try {}
		catch (Exception e) {}
	}

	public static synchronized KylieMinogue getInstance(){
		if (kylieMinogue == null) {
			throw new RuntimeException("Kylie Minogue doesn't exist!");
		} else {
			return kylieMinogue;
		}
	}

}
