package com.neuralsandwich.KylieMinogue;

public class Main {

	public static void main(String[] args) {
		KylieMinogue k1 = KylieMinogue.getInstance();
		KylieMinogue k2 = KylieMinogue.getInstance();
		
		if (k1.equals(k2)){
			System.out.println("Both Kylies are the same");
		} else {
			System.out.println("Well Looks like you can make clones!");
		}

	}

}
